# Imports
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt


# Datensatz generieren
data, classes = make_blobs(n_samples=1000, centers=5, random_state=7)

# Daten plotten
plt.scatter(data[:, 0], data[:, 1])
plt.show()

# KMeans erstellen & Trainigsdaten übergeben
kmeans = KMeans(n_clusters=5)
kmeans.fit(data)

# Klassifizierung und Zentren
label   = kmeans.labels_
center  = kmeans.cluster_centers_

# Gelernten KMeans anzeigen
plt.scatter(data[:, 0], data[:, 1], c=label)
plt.scatter(center[:, 0], center[:, 1], marker="*", color="r")

# Neuen Datensazt vorhersagen
datenpunkt  = [[8, 5]]
vorhersage  = kmeans.predict(datenpunkt)[0]
plt.scatter(datenpunkt[0][0], datenpunkt[0][1], marker="+", color="r")
print("Cluster: " + str(vorhersage) + ", Zentrum: " + str(center[vorhersage]))
plt.show()
