from multiprocessing import Process, Lock, cpu_count, current_process, Queue
from time import time


# Fkt d. Thread aufruft
def process_data(tasks, qlock):
    qlock.acquire()
    if not tasks.empty():
        data = tasks.get()
        qlock.release()
        print("Process-%s bearbeitet %s" % (current_process().pid, data))
    else:
        qlock.release()
    print("Beende Process-%s" % current_process().pid)


if __name__ == '__main__':
    # Deklaration
    cores       = cpu_count()
    lock        = Lock()
    workQueue   = Queue()

    # Aufgaben hinzufügen
    workQueue.put('Mustererkennung')
    workQueue.put('Datenbanksysteme')
    workQueue.put('Netzwerkplanung')
    workQueue.put('Programmierung')

    # Zeit Start
    start = time()

    # Erstellen der Processes
    for i in range(cores):
        p = Process(target=process_data, args=(workQueue, lock))
        p.start()
        p.join()

    # Zeit Ende
    end = time()

    # Ausgabe
    print("Programmdauer: %.2f\n" % (end - start))
