# Imports
import mysql.connector as db
#import sqlite3 as db


# Datenbankverbindung aufbauen
conn = db.connect(host="localhost",
                  user="root",
                  passwd="",
                  db="test")
# SQLite
#conn = db.connect("local.db")

# wird benötigt um mit der DB zu arbeiten
cursor = conn.cursor()

# Tabelle löschen
cursor.execute("DROP TABLE IF EXISTS kosten")

# Tabelle erstellen
cursor.execute("CREATE TABLE kosten ("
               "ID Integer,"
               "bezeichnung varchar(255),"
               "ausgaben double,"
               "PRIMARY KEY(ID))")

# Commit = Fest in der DB speichern
conn.commit()

# Datensatz einfügen
cursor.execute("INSERT INTO kosten"
               "(ID, bezeichnung, ausgaben) VALUES"
               "(001, 'Hochleistungsrechner', 110000), "
               "(002, 'Gamingrechner', 2500), "
               "(003, 'Billigrechner', 500)")
conn.commit()

# Daten aus DB auslesen
cursor.execute("SELECT * FROM kosten")
data = cursor.fetchall()
for row in data:
    print(row)

# Geziehlt Daten aus einer Tabelle löschen
cursor.execute("DELETE FROM kosten WHERE ID=002")
conn.commit()

# ALLE Daten aus einer Tabelle löschen
cursor.execute("DELETE FROM kosten")
#cursor.execute("TRUNCATE TABLE kosten")
conn.commit()

cursor.close()
conn.close()
