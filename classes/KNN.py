from math import sqrt


class KNN:
    train_dataset = None
    train_classes = None

    def __init__(self, k):
        self.k = k

    # Euklidische Distanz berechnen
    def __euklidische_Distanz(self, datenpunkt1, datenpunkt2):
        distanz = 0
        for i in range(len(datenpunkt1) - 1):
            distanz += (datenpunkt1[i] - datenpunkt2[i]) **2
        return sqrt(distanz)

    # Abstand zw Datenpunkt und allen Trainingsdatenpunkten berechnen
    def __get_nachbarn(self, test_datenpunkt):
        distanzen   = []
        nachbarn    = []

        for train_datenpunkt, train_class in zip(self.train_dataset, self.train_classes):
            dist = self.__euklidische_Distanz(train_datenpunkt, test_datenpunkt)
            distanzen.append((train_datenpunkt, train_class, dist))

        distanzen.sort(key=lambda tup: tup[2])

        for i in range(self.k):
            nachbarn.append(distanzen[i][1])
        return nachbarn

    # Algorithmus mit Daten fuellen
    def fit(self, train_dataset, train_classes):
        self.train_dataset = train_dataset
        self.train_classes = train_classes

    # Vorhersagen treffen [1x Testdatenpunkt]
    def predict(self, test_datenpunkt):
        nachbarn    = self.__get_nachbarn(test_datenpunkt)
        vorhersage  = max(set(nachbarn), key=nachbarn.count)
        return vorhersage
