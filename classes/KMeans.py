import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans


class K_Means:
    # Globale Deklaratio
    __data              = []
    __centroids         = []
    __labels            = []
    __history_centroids = []
    __k                 = None


    # Initialisierung
    def __init__(self, k, data):
        self.__k    = k
        self.__data = data


    # Euklidische Distanzberechnung
    def __euclidian(self, a, b):
        return np.linalg.norm(a - b)

    # Funktion, die zweidimensionalen Daten plottet
    def plotData(self, colors=None):
        # X & Y herrausfiltern
        x = self.__data[:, 0]
        y = self.__data[:, 1]

        # Plot, Figure erstellen & ploten
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.scatter(x, y, c=colors)
        plt.show()


    # Funktion, die ein Liniendiagramm zeichnet
    def plotClusterScore(self):
        # Iteration um passendes K zu finden
        # Ergebnisse plotten
        scores = []
        for i in range(1, 11):
            # kMeans ausführen (sklearn) und jeweiligen Score ermitteln
            print("Clusterberechnung mit K=%d" % (i))
            kmeans = KMeans(n_clusters=i)
            kmeans.fit(self.__data)
            scores.append([i, kmeans.score(self.__data)])

        # Daten in numpy Array umwandeln, X & Y Werte filtern
        scores = np.array(scores)
        x = scores[:, 0]
        y = scores[:, 1]

        # Plot, Figure erstellen & ploten
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(x, y, '-')

        # Diagrammtitel setzen
        plt.title("Cluster Wert für verschiedene K")
        plt.xlabel("K Parameter")

        # Diagrammeinstellungen % show
        plt.ylabel("Fehler")
        plt.xticks(x)
        plt.grid(linewidth=1)
        plt.show()


    # kMeans ausführen
    def predict(self, epsilon=0):
        # Deklaration
        history_centroids   = []
        iteration           = 0

        # Zeilen & Spaltenanzahl
        num_instances, num_features = self.__data.shape

        # Random Zentralpunkte setzen
        prototypes      = self.__data[np.random.randint(0, num_instances - 1, size=self.__k)]
        history_centroids.append(prototypes)
        prototypes_old  = np.zeros(prototypes.shape)

        # Leeres Distanzarray
        labels = np.zeros((num_instances, 1))

        # Erste Distanzberechnung zu 0
        norm = self.__euclidian(prototypes, prototypes_old)

        # Solange der Fehler zu groß ist
        while norm > epsilon:
            iteration += 1

            # Distanz des alter zu den neunen Zentralpunkten
            norm = self.__euclidian(prototypes, prototypes_old)
            prototypes_old = prototypes

            # finden des Zentralpunktes der kürzestens Distanz
            for index_instance, instance in enumerate(self.__data):
                dist_vec = np.zeros((self.__k, 1))

                # Distanzen vom Dataset & Zentralpunkten
                for index_prototype, prototype in enumerate(prototypes):
                    dist_vec[index_prototype] = self.__euclidian(prototype,
                                                            instance)

                # Zentralpunkt der kürzestens Dist speichern
                labels[index_instance, 0] = np.argmin(dist_vec)

            # Temp Zentroid
            tmp_prototypes = np.zeros((self.__k, num_features))

            # Bestimmen der neuen Zentralpunkte
            for index in range(len(prototypes)):
                # Clusterpunkte eines Zentroides
                instances_close = [i for i in range(len(labels)) if labels[i] == index]

                # Mittelwertbestimmung der Clusterpunkte (x & y Achsen)
                prototype = np.mean(self.__data[instances_close], axis=0)

                # Zentroid zu Temp hinzufügen
                tmp_prototypes[index, :] = prototype

            # Zentralpunkt setzen
            prototypes = tmp_prototypes
            history_centroids.append(tmp_prototypes)

        self.__centroids = prototypes
        self.__labels = labels
        self.__history_centroids = history_centroids

        return prototypes, labels

    # Plotting der Cluster & Historie
    def plotWithCentroid(self):
        # Deklaration
        colors = ['g', 'r', 'c', 'm', 'y', 'k', 'w']
        fig, ax = plt.subplots()
        history_points = []

        # Einfärben der Punkte anhand des Clusters
        for index in range(self.__k):
            # Clusterpunkte herrausfinden
            instances_close = [i for i in range(len(self.__labels))
                               if self.__labels[i] == index]

            # Punkte einfärben
            for instance_index in instances_close:
                ax.plot(self.__data[instance_index][0], self.__data[instance_index][1],
                        (colors[index] + 'o'))

        # Historie-Centroide durchgehen
        for index, centroids in enumerate(self.__history_centroids):
            # Centroide des Historie-Centroides durchgehen
            for inner, item in enumerate(centroids):
                # Erster Durchlauf, Pukte zeichnen bzw erstellen
                if index == 0:
                    history_points.append(ax.plot(item[0], item[1], 'bo', marker='*')[0])
                # Anderer Durchlauf, Position des Punkte ändern
                else:
                    # Position des Punktes ändern
                    history_points[inner].set_data(item[0], item[1])
                    print("centroids {} {}".format(index, item))

                    # Zeit zum zeichnen des t+1 Clusters
                    plt.pause(0.3)

        # Ende anzeigen
        print('Finish')
        plt.show()