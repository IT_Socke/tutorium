# Imports
from sklearn.cluster import KMeans
from skimage import io
import matplotlib.pyplot as plt
import numpy as np

# Bild
bild = io.imread("data\mina.jpg")[:, :, :3]

# Bild anzeigen
plt.imshow(bild)
plt.show()

# Axes erstellen
plt.clf()
axes1 = plt.subplot(1, 2, 1)
axes2 = plt.subplot(1, 2, 2)

# Konvert -> 1 Dim Array
data = bild.reshape(-1, 3)

# kMeans Objekt erstellen, Daten übergeben & ausführen
# n_cluster = Anzahl der Farben
kmeans = KMeans(n_clusters=8, n_init=1)
kmeans.fit(data)

# Centroiden        -> Farben
# Klassifikatoren   -> Pixel
colors  = kmeans.cluster_centers_.astype("uint8")
pixel   = kmeans.labels_

# Farben anhand der Klassifikation auslesen
pixel2show = colors[pixel]

# Shapen auf 900x1200 Pixel
pixel2show = np.array(pixel2show).reshape(900, 1200, 3)

# Bilder anzeigen
axes1.imshow(bild)
axes2.imshow(pixel2show)
plt.show()
