import queue
import threading
import time

# Deklaration
exitFlag = 0
queueLock = threading.Lock()
workQueue = queue.PriorityQueue()
threads = []

# Fkt d. Thread aufruft
def process_data(tasks):
    global exitFlag
    while not exitFlag:
        queueLock.acquire()
        if not tasks.empty():
            data = tasks.get()[1]
            queueLock.release()
            print("Thread-%s bearbeitet %s" %
                  (threading.current_thread().getName(), data))
        else:
            queueLock.release()
            exitFlag = 1
        time.sleep(2)
    print("Beende Thread-" + threading.current_thread().getName())


# Aufgaben hinzufügen
workQueue.put((1, 'Mustererkennung'))
workQueue.put((2, 'Datenbanksysteme'))
workQueue.put((3, 'Netzwerkplanung'))
workQueue.put((5, 'Programmierung'))

# Threads erstellen
for tID in range(1, 4):
    thread = threading.Thread(target=process_data,
                              name=str(tID), args=(workQueue,))
    thread.start()
    threads.append(thread)

# Aufgabe nachträglich hinzufügen
queueLock.acquire()
workQueue.put((1, 'New Task'))
queueLock.release()

# Warten auf Beendigung d. Threads
for t in threads:
    t.join()

print("Abgeschlossen")
