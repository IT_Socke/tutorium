from multiprocessing import Pool, cpu_count, current_process
from time import time


# Funktion des Pools
def quersumme(z):
    print("Process-%s -> arbeitet" % current_process().pid)
    quer = 0
    for zifferBuchstabe in z:
        quer += int(zifferBuchstabe)
    return quer


# Main Programm
if __name__ == '__main__':

    # Deklaration
    file    = open("data\zahlen.csv", "r")
    tasks   = []
    cores   = cpu_count()
    result  = []

    # Aufgaben hinzufügen
    for line in file:
        tasks = line.strip().split(';')
    file.close()

    # Zeit Start
    start = time()

    # Pool erstellen
    with Pool(cores) as p:
        result = p.map(quersumme, tasks)
        p.close()
        p.join()

    # Zeit Ende
    end = time()

    # Ausgabe
    print("Programmdauer: %.2f\n" %(end-start))
