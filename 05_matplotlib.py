# Imports
from scipy.stats import norm
from pylab import randn
import matplotlib.pyplot as plt
import numpy as np

# Linientyp anpassen
x = np.arange(-3, 3, 0.01)
plt.plot(x, norm.pdf(x), "b-")
plt.plot(x, norm.pdf(x, 1.0, 0.5), "r:")
plt.show()

# Grafik als png speichern
#plt.savefig("grafik.png", format="png")

# Achsen anpassen
fig, axes = plt.subplots()
axes.plot(x, norm.pdf(x), "b-")
axes.plot(x, norm.pdf(x, 1.0, 0.5), "r:")

# Achsenbereich angeben
axes.set_xlim([-5, 5])
axes.set_ylim([0, 1.0])

# Achsenpunkte angeben
axes.set_xticks(range(-5, 6, 1))
axes.set_yticks(np.arange(0.0, 1.1, 0.1))

# Grid anzeigen
plt.grid()

# Achsen- & Legendenbeschriftung
plt.xlabel("Merkmal xi")
plt.ylabel("Wahrscheinlichkeitsdicht")
plt.legend(["pdf", "pdf, 1.0, 0.5"], loc=4)
plt.title("Beispiel Merkmale")
plt.show()

# Tortendiagramm
werte   = [12, 55, 4, 32, 14]
farben  = ["r", "g", "b", "c", "m"]
explode = [0, 0, 0.2, 0, 0]
label   = ["Apfel", "Birne", "Kische", "Erdbeere", "Pflaume"]

plt.pie(werte, colors=farben, labels=label, explode=explode)
plt.title("Früchteverzehr")
plt.show()

# Balkendiagramm
werte   = [12, 55, 4, 32, 14]
farben  = ["r", "g", "b", "c", "m"]
plt.bar(range(5), werte, color=farben)
plt.show()

# Punktdiagramm
x = randn(500)
y = randn(500)
plt.scatter(x, y)
plt.show()

# Histogramm
werte = np.random.normal(27000, 15000, 10000)
plt.hist(werte, 50)
plt.show()
