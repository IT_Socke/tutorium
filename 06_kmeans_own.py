from sklearn.datasets import make_blobs
from classes.KMeans import K_Means


# Datensatz generieren
data, classes = make_blobs(n_samples=1000, centers=5, random_state=7)

# Objekt erstellen & Daten übergeben
kmeans  = K_Means(5, data)

# Daten ploten (ohne Klassen)
kmeans.plotData()

# Plot der Werte bei verschiedenen Clustergrößen
kmeans.plotClusterScore()

# KMeans ausführen
kmeans.predict()

# KMeans Plotten
kmeans.plotWithCentroid()
