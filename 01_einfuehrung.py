# Kommentar
""" Mehrzeiliger
Kommentar
"""

#_____________________ Arbeiten mit Variablen _____________________
# Deklaration
var_int     = 5
var_float   = 5.5
var_bool    = True
var_str     = "Hello"

# Neuzuweisung
var_bool    = var_str

# Ausgabe
print(var_int + 4)
print(var_bool + " World")


# Datenumwandlung
str1    = "11"
str2    = "14"
str3    = "0.1"
str4    = "0.3"
age     = 23

# String -> Ganzzahl
print(int(str1) + int(str2))

# String -> Gleitkommazahl
print(float(str3) + float(str4))

# Zahl -> Zeichenkette
print("Sie ist " + str(age))


#_____________________ Abfragen ___________________________________
# Deklaration
zahl    = None

# Abfrage
if zahl is None or zahl < 5:
    print("<5")
elif 5 <= zahl <= 20:
    print("5-20")
else:
    print(">20")


#_____________________ Schleifen __________________________________
# for-Schleife
for val in range(0, 10, 2):
    print(val)

# while-Schleife
cond    = True
rage    = 0

while cond:
    print(rage)
    rage += 1
    if rage > 5:
        cond = False

# do-while-Schleife (simuliert)
while True:
    print(rage)
    rage -= 1
    if rage == 0:
        break


# _____________________ Listen _____________________________________
# Deklaration & Initialisierung
myList = ["Peter", "Hans", "Gerta", "Siglinde"]

# Ausgabe
print(myList)
print(myList[0])
print(myList[len(myList)-1])

# wichtige Funktionen
myList.append("Günther")
myList.append("Tobias")
print(myList.pop())
print(myList.pop(2))
print(myList)

# Änderungen in der Liste vornehmen
myList[0] = 0
myList[1] = True
myList[2] = 5.9

# Ausgabe
print(myList)

# weitere wichtige Funktionen
myList = [[1, 2], [4, 3], [9, 7]]
myList.extend([8, 8])
myList.extend("Text")

print(myList)

# if in
if [9, 7] in myList:
    print(myList.index([9, 7]))


# Alle Elemente der Liste
myList = [1, 2, 3, 4]
for var in myList:
    print(var)


# Zip & Enumerate
myListA = [11, 12, 13, 14, 15]
myListB = [12, 13, 14, 15]

# Zip
for val_a, val_b in zip(myListA, myListB):
    print("ValA: " + str(val_a) + " & ValB: " + str(val_b))

# Enumerate
for id, val in enumerate(myListA):
    print("ID: " + str(id) + " & VAL: " + str(val))


# List Slicing
myListC   = [1, 2, 4, 7, 5, -1, -4, -3]

print(myListC[:3])
print(myListC[1:4])
print(myListC[::])
print(myListC[-3:-1])

# List Comprehension
xs  = range(1, 9)

# Übliches Vorgehen
ys = []
for x in xs:
    ys.append(x*x)

# Neues Vorgehen
ys2 = [x*x for x in xs]
print(ys)
print(ys2)


# _____________________ Datenstrukturen ____________________________
# Dictionary Deklaration & Init
myDict  = {"Berlin": "BER", "Helsinki": "HEL", "Saigon": "SGN"}

# überschreiben
myDict["Berlin"] = "DE"

# hinzufügen
myDict["Budapest"] = "BUD"

# löschen
del myDict["Berlin"]

# prüfen ob in Liste
if "Helsinki" in myDict:
    print("Enthalten")

print(myDict["Helsinki"])
print(myDict.get("Helsinki"))

# alle Elemente bearbeiten
for id, val in myDict.items():
    print(str(id) + ":" + str(val))


# Tupel Deklaration & Init
herbert     = ("Herbert", "Zimmermann", 1993, "Musterstrasse", "Musterort")
farbeRGB    = (204, 0, 153)


# Sets
text    = "Hallo Welt Hallo Mars Hallo Welt"
words   = set()
for word in text.split(" "):
    words.add(word)

print(words)


# Queue Dekla & Init
import queue

q = queue.Queue()
q.put("Hallo")
q.put("Mars")
q.put("Hallo")
q.put("Welt")

# Leeren einer Liste bzw. enfternen von Elementen
while not q.empty():
    print(q.get())

print(q.qsize())


# PriorityQueue
pq = queue.PriorityQueue()
pq.put((10, "Hallo Welt"))
pq.put((5, "Hallo"))
pq.put((15, "Mars"))
print(pq.get())


# _____________________ Exceptions _________________________________
d = [1, 2, 3, 4]
try:
    print(d[5])
except Exception as ex:
    print(ex)
finally:
    print("Programm wird beendet")


# _____________________ Arbeiten mit Dateien _______________________
file = open("D:/datei.txt", "r")

# schliessen
file.close()

# Überschreiben
file = open("D:/datei.txt", "w")
file.write("Hallo Welt - neu")
file.close()

# Hinzufügen
file = open("D:/datei.txt", "a")
file.write("\n\tHallo Welt - 2")
file.close()

# with
with open("D:/datei.txt", "a") as file:
    file.write("\n\tHallo Welt - 2")

with open("D:/datei.csv", "r") as file:
    for line in file:
        data = line.split(";")

        if data[0] == "2":
            continue

        print(data)
