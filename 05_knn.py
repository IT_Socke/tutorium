# Imports
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from classes.KNN import KNN

# Datensatz laden
iris = load_iris()

# Plotten der Klechblätter Daten
plt.scatter(iris["data"][:, 0], iris["data"][:, 1], c=iris["target"])
plt.title("Sepalum Länge und Sepalum Breite")
plt.xlabel("Sepalum Länge")
plt.ylabel("Sepalum Breite")
plt.show()

# Plotten der Kronblätter Daten
plt.scatter(iris["data"][:, 2], iris["data"][:, 3], c=iris["target"])
plt.title("Petalum Länge und Petalum Breite")
plt.xlabel("Petalum Länge")
plt.ylabel("Petalum Breite")
plt.show()

# Daten & Klassen extrahieren
data        = np.array(iris["data"][:, 2:])
classes     = np.array(iris["target"])

# Eigenimplementierung des KNN
meKNN = KNN(5)
meKNN.fit(data, classes)
vorhersage = meKNN.predict([5.5, 2.5])
print(vorhersage)

# KNN von Sklearn
from sklearn.neighbors import KNeighborsClassifier as KNN2

meKNN2 = KNN2(5)
meKNN2.fit(data, classes)
vorhersage = meKNN2.predict([[5.5, 2.5], [1.5, 2.5]])
print(vorhersage)
